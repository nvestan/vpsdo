
resource "digitalocean_tag" "foobar" {
  name = "stas_artemov_nvestan"
}

resource "digitalocean_ssh_key" "default" {
  name       = "Terraform Example"
  public_key = "${file("~/.ssh/id_rsa.pub")}"
}

# Create a new Web Droplet in the nyc2 region
resource "digitalocean_droplet" "web" {
  image  = "ubuntu-14-04-x64"
  name   = "artemov"
  region = "lon1"
  size   = "s-1vcpu-1gb"
  tags   = ["${digitalocean_tag.foobar.id}"]   
  ssh_keys = ["${digitalocean_ssh_key.default.fingerprint}"]

  connection {
   user = "root"
   
   timeout = "2m"
} 
}

output "ip" {
    value = "${digitalocean_droplet.web.ipv4_address}"
}

